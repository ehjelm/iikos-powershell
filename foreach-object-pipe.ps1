# foreach-object-pipe.ps1

$input | ForEach-Object {
   $foo += @($_)
}
Write-Output "size of foo is" $foo.Count
