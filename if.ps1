# if.ps1
if ($args.Length -ne 1) {
   $str = "usage: " +
          $MyInvocation.InvocationName +
          " <argument>"
   Write-Output $str
}
