# assoc-array.ps1

$user=@{
       "frodeh" = "Frode Haug";
       "ivarm"  = "Ivar Moe"
       }
$user+=@{"lailas"="Laila Skiaker"}
Write-Output $user["ivarm"]  # print Ivar Moe
Write-Output @user           # print array values
Write-Output $user.Keys      # print array keys
Write-Output $user.Count     # print length of array
