# foreach.ps1

foreach ($i in Get-ChildItem) {
   Write-Output $i.Name
}

# with associative arrays

$user=@{
       "frodeh" = "Frode Haug";
       "monicas"  = "Monica Strand";
       "ivarm"  = "Ivar Moe"
       }
foreach ($key in $user.Keys) {
   Write-Output $user[$key]
}
