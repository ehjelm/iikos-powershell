# if-num-string.ps1

if ($args.Count -ne 2) {
   $str = "usage: " +
          $MyInvocation.InvocationName +
          " <argument> <argument>"
   Write-Output $str
   exit 1
} elseif ($args[0] -gt $args[1]) {
   Write-Output "$($args[0]) larger than $($args[1])"
} else {
   $str = "$($args[0]) smaller than or" +
          " equal to $($args[1])"
   Write-Output $str
}
if (Test-Path $args[0]) {
   if (!(Get-Item $args[0]).PSIsContainer) {
      Write-Output $args[0] "is a file"
   }
}
