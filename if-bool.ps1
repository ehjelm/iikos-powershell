# if-bool.ps1

if ((1 -eq 2) -and (1 -eq 1) -or (1 -eq 1)) {
   Write-Output "And has precedence"
} else {
   Write-Output "Or has precedence"
}

# force OR precedence:

if ((1 -eq 2) -and ((1 -eq 1) -or (1 -eq 1))) {
   Write-Output "And has precedence"
} else {
   Write-Output "Or has precedence"
}
