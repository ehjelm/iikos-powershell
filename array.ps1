# array.ps1

$os=@("linux", "windows")
$os+=@("mac")
Write-Output $os[1]     # print windows
Write-Output $os        # print array values
Write-Output $os.Count  # length of array
