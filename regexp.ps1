# regexp.ps1

$input | ForEach-Object {
   if ($_ -match
       "^[A-Za-z0-9._-]+@([A-Za-z0-9.-]+)$") {
      Write-Output "Valid email", $matches[0]
      Write-Output "Domain is", $matches[1]
   } else {
      Write-Output "Invalid email address!"
   }
}
