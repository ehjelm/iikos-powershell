# switch.ps1

$short = @{ yes="y"; nope="n" }
$ans = Read-Host
switch ($ans) {
   yes { Write-Output "yes" }
   nope { Write-Output "nope"; break }
   {$short.ContainsKey("$ans")} `
      { Write-Output $short[$ans] }
   default {Write-Output "$ans`???"}
}
