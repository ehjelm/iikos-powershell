# input-commands.ps1

$name=(Get-CimInstance Win32_OperatingSystem).Name
$kernel=(Get-CimInstance `
         Win32_OperatingSystem).Version
Write-Output "I am running on $name, version" `
           "$kernel in $(Get-Location)"
