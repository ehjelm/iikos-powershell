# struct.ps1

$myhost=New-Object PSObject -Property `
        @{os="";
          sw=@();
          user=@{}
         }
$myhost.os="linux"
$myhost.sw+=@("gcc","flex","vim")
$myhost.user+=@{
                "frodeh"="Frode Haug";
                "monicas"="Monica Strand"
               }
Write-Output $myhost.os
Write-Output $myhost.sw[2]
Write-Output $myhost.user["monicas"]
