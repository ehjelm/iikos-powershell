# while.ps1

while ($i -le 3) {
   Write-Output $i
   $i++
}

# something more useful:

$file=Get-ChildItem
$i=0
while ($i -lt $file.Count) {
   if (!(Get-Item $file[$i]).PSIsContainer) {
      Write-Output $file[$i].Name "is a file"
   } else {
      Write-Output $file[$i].Name "is a directory"
   }
   $i++
}
